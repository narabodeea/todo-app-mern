const db = require('../persistence');

module.exports = async (req, res) => {
    await db.removeItem(req.params.id);
    console.log('TODO-APP delete: ' + req.params.id);
    res.sendStatus(200);
};
