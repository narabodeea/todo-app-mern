const express = require('express');
const app = express();
const db = require('./persistence');
const getItems = require('./routes/getItems');
const addItem = require('./routes/addItem');
const updateItem = require('./routes/updateItem');
const deleteItem = require('./routes/deleteItem');
const client = require('prom-client');
const cors = require('cors')

app.use(require('body-parser').json());
app.use(express.static(__dirname + '/static'));

app.get('/items', getItems);
app.post('/items', addItem);
app.put('/items/:id', updateItem);
app.delete('/items/:id', deleteItem);

const register = new client.Registry();
client.collectDefaultMetrics({ register });

const counter = new client.Counter({
    name: 'http_requests_total',
    help: 'Total number of HTTP requests',
    labelNames: ['method'],
});

const histogram = new client.Histogram({
    name: 'http_request_duration_seconds',
    help: 'Duration of HTTP requests in seconds',
    labelNames: ['route'],
    bucket: [0.1, 0.3, 0.5, 0.7, 1, 3, 5, 7, 10],
});

app.use((req, res, next) => {
    counter.inc({ method: req.method });

    const route = req.route ? req.route.path : req.url;
    const end = histogram.startTimer({ route });
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept',
    );

    res.on('finish', () => {
        end();
    });

    next();
});

app.get('/metrics', async (req, res) => {
    res.set('Content-Type', register.contentType);
    res.end((await register.metrics()).toString());
});

db.init()
    .then(() => {
        let port = process.env.APP_PORT || 3000;
        app.listen(port, () =>
            console.log('TODO-MERN is listening on port ' + port),
        );
    })
    .catch(err => {
        console.error(err);
        process.exit(1);
    });

const gracefulShutdown = () => {
    db.teardown()
        .catch(() => {})
        .then(() => process.exit());
};

process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);
process.on('SIGUSR2', gracefulShutdown); // Sent by nodemon
